<?php

namespace App\Services;

class CurrencyExchangeService
{
    protected $currencies;

    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    public function exchange(string $source, string $target, float $amount): string
    {
        if (!isset($this->currencies[$source])) {
            throw new \InvalidArgumentException('Invalid source currency.');
        }

        if (!isset($this->currencies[$source][$target])) {
            throw new \InvalidArgumentException('Invalid target currency.');
        }

        $rate = $this->currencies[$source][$target];
        if (!is_numeric($rate)) {
            throw new \TypeError('Invalid currency rate.');
        }

        $targetAmount = round($amount * $rate, 2);

        return number_format($targetAmount, 2);
    }
}
