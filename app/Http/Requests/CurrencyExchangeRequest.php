<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CurrencyExchangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $currencies = array_keys(config('currencies'));

        return [
            'source' => ['required', 'string', Rule::in($currencies)],
            'target' => ['required', 'string', Rule::in($currencies)],
            'amount' => ['required', 'regex:/^(\d{1,3}(,\d{3})*|\d+)(\.\d+)?$/'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'msg'    => 'parameter error',
                'errors' => $validator->errors()
            ], 422)
        );
    }
}
