<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyExchangeRequest;
use App\Services\CurrencyExchangeService;

class CurrencyController extends Controller
{
    protected $service;

    public function __construct(CurrencyExchangeService $service)
    {
        $this->service = $service;
    }

    public function exchange(CurrencyExchangeRequest $request)
    {
        $source = $request->source;
        $target = $request->target;
        $amount = str_replace(',', '', $request->amount);

        try {
            $amount = $this->service->exchange($source, $target, $amount);
        } catch (\Exception $e) {
            return response()->json([
                'msg' => 'server error'
            ], 500);
        }

        return response()->json([
            'msg' => 'success',
            'amount' => $amount
        ]);
    }
}
