FROM php:8.1-apache

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y \
    vim \
    git \
    unzip \
    libzip-dev \
    && docker-php-ext-install zip

# mod_rewrite for URL rewrite and mod_headers for .htaccess extra headers
RUN a2enmod rewrite headers

# install composer
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# copy project files
COPY . /var/www/html
