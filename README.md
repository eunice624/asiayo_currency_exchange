# AsiaYo API 實作測驗

## 安裝步驟
1. clone 本專案
```bash
$ https://gitlab.com/eunice624/asiayo_currency_exchange.git
```

2. 在 `/etc/hosts` 加入自訂 Domain
```
127.0.0.1 asiayo-currency-exchange
```

3. 執行 docker-compose
```bash
$ cd asiayo_currency_exchange
$ docker-compose up -d
```

4. 進入 container
```bash
$ docker-compose exec app bash
```

5. 在 container 中安裝相依套件
```bash
$ composer install
```

## 使用 API
> 本專案的服務運行在 8080 port。  
> 需將 request 發送到 `http://asiayo-currency-exchange:8080`

### 匯率轉換 API
- Path: `/api/v1/currencies/exchange`
- Content-Type: `application/json`
- HTTP Method: `GET`
- Request Parameters:
  - source: 原始幣別
  - target: 目標幣別
  - amount: 原始金額
- Example:
  - Request URL: `http://asiayo-currency-exchange:8080/api/v1/currencies/exchange?source=USD&target=JPY&amount=1,525`
  - Response:
    ```json
    {
        "msg": "success",
        "amount": "170,496.53"
    }
    ```

---

## 題目
請使用 Laravel 實作一個提供匯率轉換的 API
- 此 API 需滿足以下條件
  - 請使用 docker 包裝您的環境。若未使用 docker 或 docker-compose 不予給分。
  - Controller 接收 GET 參數時，請使用 Laravel 的 FormRequest，若未使用 FormRequest 物件，不予給分。
  - 需驗證使用者輸入的值。 source、target 為字串，amount 輸入時無論有無千分位皆可接受。例如「1,525」或「1525」皆可。
  - 請新增 CurrencyExchangeService，並且使用依賴注入(DependencyInjection, DI) 以下靜態匯率資料。此資料若未依賴注入至 CurrencyExchangeService 不予給分。
  - 轉換後的結果，請四捨五入到小數點第二位。
  - 轉換後的結果，請加上半形逗點作為千分位表示，每三個位數一點。
  - CurrencyExchangeService 必須包含(但不限於)以下測試案例:
    - 若輸入的 source 或 target 系統並不提供時的案例
    - 若輸入的金額為非數字或無法辨認時的案例
    - 輸入的數字需四捨五入到小數點第二位，並請提供覆蓋有小數與沒有小數的多種案例
  - 實作結果需以 GitHub、GitLab、Bitbucket 或其他程式碼托管網站連結呈現。若未使用不予給分。


- 輸入範例：  
```
?source=USD
&target=JPY
&amount=1,525
```

- 輸出範例：  
```
{
	"msg": "success"
	"amount": "170,496.53"
}
```

- 請使用以下匯率資料：  
```
{
    "currencies": {
        "TWD": {
            "TWD": 1,
            "JPY": 3.669,
            "USD": 0.03281
        },
        "JPY": {
            "TWD": 0.26956,
            "JPY": 1,
            "USD": 0.00885
        }, "USD":
        {
            "TWD": 30.444,
            "JPY": 111.801,
            "USD": 1
        }
    }
}
```
