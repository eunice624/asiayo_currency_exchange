<?php

namespace Tests\Unit;

use App\Services\CurrencyExchangeService;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CurrencyExchangeServiceTest extends TestCase
{
    use WithFaker;

    protected array $currencies;
    protected CurrencyExchangeService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->currencies = [
            'TWD' => [
                'TWD' => 1,
                'JPY' => 3.669,
                'USD' => 0.03281,
                'TYPE_ERROR' => 'TYPE_ERROR'
            ],
            'JPY' => [
                'TWD' => 0.26956,
                'JPY' => 1,
                'USD' => 0.00885
            ],
            'USD' => [
                'TWD' => 30.444,
                'JPY' => 111.801,
                'USD' => 1
            ]
        ];

        // Mocking currencies config data
        $mockService = new CurrencyExchangeService($this->currencies);
        $this->app->instance(CurrencyExchangeService::class, $mockService);
        $this->service = app(CurrencyExchangeService::class);
    }

    public function testExchangeWithInvalidSourceCurrency()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid source currency.');

        $source = 'INVALID';
        $target = 'USD';
        $amount = 100;

        $this->service->exchange($source, $target, $amount);
    }

    public function testExchangeWithInvalidTargetCurrency()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid target currency.');

        $source = 'TWD';
        $target = 'INVALID';
        $amount = 100;

        $this->service->exchange($source, $target, $amount);
    }

    public function testExchangeWithTypeErrorCurrencyRate()
    {
        $this->expectException(\TypeError::class);
        $this->expectExceptionMessage('Invalid currency rate.');

        $source = 'TWD';
        $target = 'TYPE_ERROR';
        $amount = 100;

        $this->service->exchange($source, $target, $amount);
    }

    public function testExchangeWithTypeErrorAmount()
    {
        $this->expectException(\TypeError::class);

        $source = 'TWD';
        $target = 'JPY';
        $amount = 'INVALID';

        $this->service->exchange($source, $target, $amount);
    }

    /**
     * @dataProvider amountProvider
     */
    public function testExchangeSuccess(float $amount)
    {
        $currencyKeys = array_keys($this->currencies);
        $source = $this->faker->randomElement($currencyKeys);
        $target = $this->faker->randomElement($currencyKeys);
        $rate = $this->currencies[$source][$target];

        $result = $this->service->exchange($source, $target, $amount);

        $expectedAmount = round($amount * $rate, 2);
        $this->assertEquals(number_format($expectedAmount, 2), $result);
    }

    public static function amountProvider()
    {
        $faker = \Faker\Factory::create();

        return [
            'amount with decimal point' => [$faker->randomFloat()],
            'amount without decimal point' => [$faker->randomNumber()],
            'amount is zero' => [0]
        ];
    }
}
